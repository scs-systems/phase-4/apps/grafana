cat <<EOT | kubectl apply -n web --force -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $CI_PROJECT_NAME
  namespace: web
  labels:
    app: $CI_PROJECT_NAME
spec:
  replicas: 1
  selector:
    matchLabels:
      app: $CI_PROJECT_NAME
  template:
    metadata:
      labels:
        app: $CI_PROJECT_NAME
    spec:
      containers:
      - name: $CI_PROJECT_NAME
        image: gitlab.scsuk.net:5005/scs-systems/ext_registry/grafana/grafana:6.2.5
        ports:
        - containerPort: 80
        env:
        - name: GF_SECURITY_ADMIN_PASSWORD
          value: "admintmp"
        - name: GF_AUTH_ANONYMOUS_ENABLED
          value: "true"
        - name: GF_AUTH_ANONYMOUS_ORG_NAME
          value: "Main Org."
        - name: GF_AUTH_ANONYMOUS_ORG_ROLE
          value: "Viewer"
        volumeMounts:
            - mountPath: /var/lib/grafana
              name: grafana-data-persistent-storage
      volumes:
      - name: grafana-data-persistent-storage
        hostPath:
          path: /storage/static/grafana/data
          type: Directory
      nodeSelector:
        kubernetes.io/hostname: jam4.local

---

apiVersion: v1
kind: Service
metadata:
  name: $CI_PROJECT_NAME
  namespace: web
spec:
  selector:
    app: $CI_PROJECT_NAME
  ports:
    - protocol: TCP
      port: 80
      targetPort: 3000
EOT

echo
kubectl -n web get service $CI_PROJECT_NAME
echo
kubectl -n web get deployment $CI_PROJECT_NAME
echo
kubectl -n web get pods -l app=$CI_PROJECT_NAME -o wide
